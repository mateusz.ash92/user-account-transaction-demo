DROP TABLE if exists user_account;
DROP TABLE if exists transaction;
DROP TABLE if exists user;
DROP TABLE if exists account;
DROP TABLE if exists transaction_category;

CREATE TABLE user
(
    user_id  VARCHAR(50),
    name     VARCHAR(50),
    surname  VARCHAR(50),
    login    VARCHAR(50),
    password VARCHAR(50),
    role     VARCHAR(50),
    email    VARCHAR(50),
    PRIMARY KEY (user_id)
);
CREATE TABLE transaction_category
(
    transaction_category_id VARCHAR(50),
    name                    VARCHAR(50),
    PRIMARY KEY (transaction_category_id)
);
CREATE TABLE account
(
    account_id VARCHAR(50),
    name       VARCHAR(50),
    balance    NUMERIC,
    PRIMARY KEY (account_id)
);
CREATE TABLE user_account
(
    user_id    VARCHAR(50),
    account_id VARCHAR(50),
    FOREIGN KEY (user_id) REFERENCES user (user_id),
    FOREIGN KEY (account_id) REFERENCES account (account_id),
    PRIMARY KEY (user_id, account_id)
);
CREATE TABLE transaction
(
    transaction_id          VARCHAR(50),
    description             VARCHAR(250),
    amount                  NUMERIC,
    transaction_category_id VARCHAR(50),
    account_id              VARCHAR(50),
    user_id                 VARCHAR(50),
    date                    TIMESTAMP,
    PRIMARY KEY (transaction_id),
    FOREIGN KEY (account_id) REFERENCES account (account_id),
    FOREIGN KEY (transaction_category_id) references transaction_category (transaction_category_id),
    FOREIGN KEY (user_id) references user (user_id)
);
