INSERT INTO user (user_id, name, surname, login, password, role, email) values
('1', 'Adam', 'Kowalski', 'akowalksi', 'admin', 'admin', 'admin@admin.pl'),
('2', 'Marcin', 'Nowak', 'mnowak', 'admin', 'user', 'mnowak@admin.pl'),
('3', 'Marta', 'Kowalska', 'mkowalkska', 'user', 'admin', 'mkowalska@admin.pl');

INSERT INTO account (account_id, name, balance) values
('1', 'Konto Kowalskiego', 100),
('2', 'Konto Nowaka', 101),
('3', 'Konto Kowalskiej', 12000),
('4', 'Drugie konto Kowalskiego', 120000);

INSERT INTO user_account (user_id, account_id) values
('1', '1'),
('2', '2'),
('3', '3'),
('3', '1'),
('1', '4');

INSERT INTO transaction_category (transaction_category_id, name) values
('1', 'dom'),
('2', 'samochód'),
('3', 'zdrowie'),
('4', 'hobby');

INSERT INTO transaction (transaction_id, description, amount, transaction_category_id, account_id, user_id) values
('1', 'wydatki na dom', 35, '1', '1', '1'),
('2', 'wydatki na samochód', 80.99, '2', '1', '3'),
('3', 'wydatki w aptece', 1500, '3', '3', '3'),
('4', 'gra na PS4', 99.99, '4', '2', '2');







