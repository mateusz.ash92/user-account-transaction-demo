<%@ page import="com.mcwik.pojos.Transaction" %>
<%@ page import="java.util.List" %>
<%@ page import="com.mcwik.daos.TransactionDAO" %><%--
  Created by IntelliJ IDEA.
  User: ubuntu
  Date: 03.10.2019
  Time: 18:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>All Transactions</title>
</head>
<body>
<%
    List<Transaction> allTransactions = (List<Transaction>) request.getAttribute("transactions");
%>
<h1>All Transactions</h1>
<br>
<table>
    <tr>
        <th>ID</th>
        <th>Description</th>
        <th>Amount</th>
        <th>CategoryID</th>
        <th>AccountID</th>
        <th>UserID</th>
        <th>Date</th>
    </tr>
    <% for (Transaction transaction : allTransactions) {
    %>
    <tr>
        <th><%=transaction.getId()%></th>
        <th><%=transaction.getDescription()%></th>
        <th><%=transaction.getAmount()%></th>
        <th><%=transaction.getCategoryId()%></th>
        <th><%=transaction.getAccountId()%></th>
        <th><%=transaction.getUserId()%></th>
        <th><%=transaction.getDate()%></th>
    </tr>
    <%}%>
    <br>
</table>
<hr>
<a href="homepage.html"><< Back to homepage</a>
</body>
</html>
