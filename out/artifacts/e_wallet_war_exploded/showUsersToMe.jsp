<%@ page import="com.mcwik.services.UserService" %>
<%@ page import="com.mcwik.pojos.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: ubuntu
  Date: 26.09.2019
  Time: 17:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
</head>
<body>
<h1>Registered Users</h1>
<br>
<table>
    <tr>
        <th>ID</th>
        <th>Imie</th>
        <th>Nazwisko</th>
        <th>Login</th>
        <th>Rola</th>
        <th>Email</th>
        <th>Accounts</th>
        <th>Transactions</th>
    </tr>
    <%
        List<User> users = (List<User>) request.getAttribute("users");
        response.setContentType("text/html;charset=UTF-8");

        for (User user : users) {
            String id = user.getId();
            String name = user.getName();
            String surname = user.getSurname();
            String login = user.getLogin();
            String role = user.getRole();
            String email = user.getEmail();

            out.write("<tr>\n" +
                    "<td>" + id + "</td>\n" +
                    "<td>" + name + "</td>\n" +
                    "<td>" + surname + "</td>\n" +
                    "<td>" + login + "</td>\n" +
                    "<td>" + role + "</td>\n" +
                    "<td>" + email + "</td>\n" +
                    "<td><a href=delete-account?id=" + id +">Delete</a></td>\n" +
                    "<td><a href=edit-user?id=" + id +">Edit</a></td>\n" +
                    "<td><a href=userRole?id=" + id +">Set user role</a></td>\n"  +
                    "<td><a href=adminRole?id=" + id +">Set admin role</a></td>\n" +

                    "</tr>\n");
        }
    %>
</table>
<a href="homepage.html">Homepage</a>
</body>
</html>
