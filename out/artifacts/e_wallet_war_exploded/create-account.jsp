<%--
  Created by IntelliJ IDEA.
  User: ubuntu
  Date: 05.10.2019
  Time: 10:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create account</title>
</head>
<body>

<h1>Create new account</h1>

<form method="post" action="create-account">
    <label>
        Name:
        <input type="text" name="name">
    </label>
    <br>

    <label>
        Balance:
        <input type="number" name="balance" value="0">
    </label>
    <br>

    <input type="submit" value="Create">

</form>

</body>

</html>
