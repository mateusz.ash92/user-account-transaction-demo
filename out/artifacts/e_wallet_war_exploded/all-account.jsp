<%@ page import="com.mcwik.pojos.Account" %>
<%@ page import="java.util.List" %>
<%@ page import="com.mcwik.daos.AccountDAO" %><%--
  Created by IntelliJ IDEA.
  User: ubuntu
  Date: 03.10.2019
  Time: 18:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>All Accounts</title>
</head>
<body>
<%
    List<Account> allAccounts = (List<Account>) request.getAttribute("accounts");
%>
<h1>All Accounts</h1>
<br>
<table>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Balance</th>
    </tr>
    <% for (Account account : allAccounts) {
    %>
    <tr>
        <th><%=account.getId()%></th>
        <th><%=account.getName()%></th>
        <th><%=account.getBalance()%></th>
        <th><a href="add-transaction.jsp?id=<%=account.getId()%>">Add Transaction</a></th>
    </tr>
    <%}%>
    <br>
</table>
<hr>
<a href="homepage.html"><< Back to homepage</a>
</body>
</html>
