<%@ page import="com.mcwik.services.AccountService" %>
<%@ page import="com.mcwik.pojos.Account" %>
<%@ page import="java.util.List" %>
<%@ page import="com.mcwik.services.UserService" %>
<%@ page import="com.mcwik.pojos.User" %>
<%@ page import="java.math.BigDecimal" %>
<%@ page import="com.mcwik.daos.AccountDAO" %><%--
  Created by IntelliJ IDEA.
  User: ubuntu
  Date: 01.10.2019
  Time: 17:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Your Accounts</title>
</head>
<body>
<%
    List<Account> accounts = (List<Account>) request.getAttribute("accounts");
    BigDecimal sum = (BigDecimal) request.getAttribute("sum");
%>
<h1>User Accounts</h1>
<br>
<table>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Balance</th>
    </tr>
    <% for (Account account : accounts) {
    sum = sum.add(account.getBalance());
    %>
    <tr>
        <th><%=account.getId()%></th>
        <th><%=account.getName()%></th>
        <th><%=account.getBalance()%></th>
        <th><a href=add-transaction.jsp?id=<%=account.getId()%>>Add Transaction</a></th>
    </tr>
    <%}%>
    <br>
    <tr>
        <th></th>
        <th></th>
        <th>SUMMARY:</th>
        <th><%=sum%></th>
    </tr>
</table>
<hr>
<a href="homepage.html"><< Back to homepage</a>
</body>
</html>
