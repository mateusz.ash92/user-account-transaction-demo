package com.mcwik.pojos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Account {
    private String id;
    private String name;
    private BigDecimal balance = BigDecimal.ZERO;
    private List<String> userIds = new ArrayList<>();
    private List<String> transactionIds = new ArrayList<>();

    public Account(String id, String name, BigDecimal balance, String userId) {
        this.id = id;
        this.name = name;
        this.balance = balance;
        this.userIds.add(userId);
    }
    public Account(String id, String name, BigDecimal balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }

    public List<String> getTransactionIds() {
        return transactionIds;
    }

    public void setTransactionIds(List<String> transactionIds) {
        this.transactionIds = transactionIds;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                ", userIds=" + userIds +
                ", transactionIds=" + transactionIds +
                '}';
    }
}
