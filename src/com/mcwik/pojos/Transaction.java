package com.mcwik.pojos;

import java.math.BigDecimal;

public class Transaction {
    private String id;
    private String userId;
    private String accountId;
    private BigDecimal amount = BigDecimal.ZERO;
    private String description;
    private String categoryId;
    private String date;

    public Transaction(String id, String userId, String accountId, BigDecimal amount, String description, String categoryId, String date) {
        this.id = id;
        this.userId = userId;
        this.accountId = accountId;
        this.amount = amount;
        this.description = description;
        this.categoryId = categoryId;
        this.date = date;
    }

    public Transaction(String id, String description, BigDecimal amountDecimal, String userId, String accountId) {
        this.id = id;
        this.description = description;
        this.amount = amountDecimal;
        this.userId = userId;
        this.accountId = accountId;
    }
    public Transaction(String id, String description, BigDecimal amountDecimal, String transactionCategoryId, String userId, String accountId) {
        this.id = id;
        this.description = description;
        this.amount = amountDecimal;
        this.categoryId = transactionCategoryId;
        this.userId = userId;
        this.accountId = accountId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", accountId='" + accountId + '\'' +
                ", amount=" + amount +
                ", description='" + description + '\'' +
                ", date=" + date +
                '}';
    }
}
