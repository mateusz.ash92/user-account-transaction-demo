package com.mcwik.pojos;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String id;
    private String name;
    private String surname;
    private String login;
    private String password;
    private String role;
    private String email;
    private List<String> accountIds = new ArrayList<>();
    private List<String> transactionIds = new ArrayList<>();

    public User(String id, String name, String surname, String login, String password, String role, String email) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.role = role;
        this.email = email;
    }

    public User(String id, String name, String surname, String login, String password, String role, String email, List<String> accountIds, List<String> transactionIds) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.role = role;
        this.email = email;
        this.accountIds = accountIds;
        this.transactionIds = transactionIds;
    }

    public List<String> getAccountIds() {
        return accountIds;
    }

    public void setAccountIds(List<String> accountIds) {
        this.accountIds = accountIds;
    }

    public List<String> getTransactionIds() {
        return transactionIds;
    }

    public void setTransactionIds(List<String> transactionIds) {
        this.transactionIds = transactionIds;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "id: " + id + " name: " + name + " surname: " + surname + " login: "
                + login + " password: " + password + " role: " + role + " email: " + email + "\n";
    }
}
