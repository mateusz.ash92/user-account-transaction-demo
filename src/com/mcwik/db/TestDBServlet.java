package com.mcwik.db;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;

@WebServlet("/testDB")
public class TestDBServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Connection connection = DatabaseConnector.createConnection();

        if (connection == null) {
            resp.getWriter().write("ERROR WITH DB CONNECTION");
        } else {
            resp.getWriter().write("DB CONNECTION WORKS");
        }
    }
}
