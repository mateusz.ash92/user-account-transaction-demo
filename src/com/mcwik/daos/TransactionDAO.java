package com.mcwik.daos;

import com.mcwik.db.DatabaseConnector;
import com.mcwik.pojos.Account;
import com.mcwik.pojos.Transaction;
import com.mcwik.services.AccountService;
import com.mcwik.services.TransactionService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
public class TransactionDAO {
    @EJB
    private AccountService accountService;
    @EJB
    private AccountDAO accountDAO;

    public List<Transaction> findAll() {
        List<Transaction> allTransactions = new ArrayList<>();
        Connection connection = DatabaseConnector.createConnection();
        String sql = "select * from transaction;";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                allTransactions.add(getTransaction(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allTransactions;
    }

    public List<Transaction> findById(String id) {
        List<Transaction> transactionsByUserId = new ArrayList<>();
        Connection connection = DatabaseConnector.createConnection();
        String sql = "select * from transaction where user_id = ?";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                transactionsByUserId.add(getTransaction(resultSet));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return transactionsByUserId;
    }

    private Transaction getTransaction(ResultSet resultSet) throws SQLException {
        return new Transaction(resultSet.getString("transaction_id"),
                resultSet.getString("user_id"), resultSet.getString("account_id"),
                resultSet.getBigDecimal("amount"), resultSet.getString("description"),
                resultSet.getString("transaction_category_id"), resultSet.getString("date"));
    }

    public void addNew(Transaction transaction) {
        Connection connection = DatabaseConnector.createConnection();
        String sql = "insert into transaction values (?,?,?,?,?,?,?);";
        Account account = accountService.findByAccountId(transaction.getAccountId());
        BigDecimal oldBalance = account.getBalance();
        BigDecimal transactionAmount = transaction.getAmount();
        account.setBalance(oldBalance.add(transactionAmount));
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, transaction.getId());
            preparedStatement.setString(2, transaction.getDescription());
            preparedStatement.setBigDecimal(3, transaction.getAmount());
            preparedStatement.setString(4, transaction.getCategoryId());
            preparedStatement.setString(5, transaction.getAccountId());
            preparedStatement.setString(6, transaction.getUserId());
            preparedStatement.setDate(7, new java.sql.Date(new Date().getTime()));
            accountDAO.updateBalance(transaction.getAccountId(), account.getBalance());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Transaction> getForAccount(String id) {
        Connection connection = DatabaseConnector.createConnection();
        String sql = "select * from transaction where account_id=?;";
        List<Transaction> forAccount = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                forAccount.add(createTransactionFromResultSet(id, resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return forAccount;

    }

    private Transaction createTransactionFromResultSet(String id, ResultSet resultSet) throws SQLException {
        return new Transaction(resultSet.getString("transaction_id"), resultSet.getString("description"),
                resultSet.getBigDecimal("amount"), resultSet.getString("transaction_category_id"),
                resultSet.getString("user_id"), resultSet.getString(id));
    }
}
