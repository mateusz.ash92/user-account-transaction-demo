package com.mcwik.daos;

import com.mcwik.db.DatabaseConnector;
import com.mcwik.pojos.User;

import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class UserDAO {

    public UserDAO() {
    }

    public List<User> findAll() {
        Connection connection = DatabaseConnector.createConnection();
        List<User> allUsers = new ArrayList<>();
        String sql = "select * from user;";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                allUsers.add(resultSetToUser(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allUsers;
    }

    public void addUser(User user) {
        Connection connection = DatabaseConnector.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into user values (?,?,?,?,?,?,?);");
            preparedStatement.setString(1, user.getId());
            preparedStatement.setString(2, user.getName());
            preparedStatement.setString(3, user.getSurname());
            preparedStatement.setString(4, user.getLogin());
            preparedStatement.setString(5, user.getPassword());
            preparedStatement.setString(6, user.getRole());
            preparedStatement.setString(7, user.getEmail());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User findById(String id)  {
        Connection connection = DatabaseConnector.createConnection();
        String sql = "select * from user where user_id = ?;";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
               return resultSetToUser(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private User resultSetToUser(ResultSet resultSet) throws SQLException {
        return new User(resultSet.getString("user_id"), resultSet.getString("name"), resultSet.getString("surname"),
                resultSet.getString("login"), resultSet.getString("password"), resultSet.getString("role"),
                resultSet.getString("email"));
    }

    public User findUserByLogin(String login) {
        Connection connection = DatabaseConnector.createConnection();
        String sql = "select * from user where login = ?";
        User userByLogin = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            userByLogin = resultSetToUser(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userByLogin;
    }

    public void updateUser(String id, String name, String surname, String login, String password, String role,
                                  String email) {
        Connection connection = DatabaseConnector.createConnection();
        String sql = "update user SET user_id = ?, name = ?, surname = ?, login = ?, password = ?, role = ?, email = ?" +
                " where user_id = ?;";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, id);
            preparedStatement.setString(2, name);
            preparedStatement.setString(3, surname);
            preparedStatement.setString(4, login);
            preparedStatement.setString(5, password);
            preparedStatement.setString(6, role);
            preparedStatement.setString(7, email);
            preparedStatement.setString(8, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteUserById(String id) {
        Connection connection = DatabaseConnector.createConnection();
        String sql = "delete from user where user_id = ?;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
