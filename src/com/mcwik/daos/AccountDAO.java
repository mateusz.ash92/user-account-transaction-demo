package com.mcwik.daos;

import com.mcwik.db.DatabaseConnector;
import com.mcwik.pojos.Account;

import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class AccountDAO {

    public AccountDAO() {
    }

    public List<Account> findAll() {
        Connection connection = DatabaseConnector.createConnection();
        List<Account> allAccounts = new ArrayList<>();
        String sql = "select * from account";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                allAccounts.add(new Account(resultSet.getString("account_id"), resultSet.getString("name"),
                        resultSet.getBigDecimal("balance")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allAccounts;
    }


    public List<Account> findByUserId(String id) {
        List<Account> accountsById = new ArrayList<>();
        Connection connection = DatabaseConnector.createConnection();
        String sql = "select * from account a join user_account u on a.account_id = u.account_id where user_id = ?";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                accountsById.add(new Account(resultSet.getString("account_id"),
                        resultSet.getString("name"), resultSet.getBigDecimal("balance"),
                        resultSet.getString("user_id")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return accountsById;
    }

    public Account findByAccountId(String id) {
        Connection connection = DatabaseConnector.createConnection();
        String sql = "select * from account where account_id = ?";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new Account(resultSet.getString("account_id"), resultSet.getString("name"),
                        resultSet.getBigDecimal("balance"), null);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addNew(Account account, String userId) {
        Connection connection = DatabaseConnector.createConnection();
        String sql = "insert into account values (?,?,?);";
        String sql2 = "insert into user_account values (?,?);";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, account.getId());
            preparedStatement.setString(2, account.getName());
            preparedStatement.setBigDecimal(3, account.getBalance());
            preparedStatement.executeUpdate();
            PreparedStatement preparedStatement1 = connection.prepareStatement(sql2);
            preparedStatement1.setString(1, userId);
            preparedStatement1.setString(2, account.getId());
            preparedStatement1.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateBalance(String accountId, BigDecimal amount) {
        Connection connection = DatabaseConnector.createConnection();
        String sql = "update account set balance = ? where account_id = ?;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setBigDecimal(1, amount);
            preparedStatement.setString(2, accountId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //    public static List<Account> findAll() {
//        Connection connection = DatabaseConnector.createConnection();
//        List<Account> allAccounts = new ArrayList<>();
//        String sql = "select * from account a join user_account u on a.account_id = u.account_id";
//        PreparedStatement preparedStatement = null;
//        try {
//            preparedStatement = connection.prepareStatement(sql);
//            ResultSet resultSet = preparedStatement.executeQuery();
//            while (resultSet.next()) {
//                allAccounts.add(new Account(resultSet.getString("account_id"), resultSet.getString("name"),
//                        resultSet.getBigDecimal("balance"), resultSet.getString("user_id")));
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return allAccounts;
//    }
}
