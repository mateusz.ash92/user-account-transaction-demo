package com.mcwik.daos;

import com.mcwik.db.DatabaseConnector;
import com.mcwik.pojos.TransactionCategory;
import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class TransactionCategoryDAO {
    public List<TransactionCategory> getAll() {
        List<TransactionCategory> transactionCategories = new ArrayList<>();
        Connection connection = DatabaseConnector.createConnection();
        String sql = "select * from transaction_category;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                transactionCategories.add(new TransactionCategory(resultSet.getString("transaction_category_id"),
                        resultSet.getString("name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return transactionCategories;
    }

    public void deleteById(String id) {
        Connection connection = DatabaseConnector.createConnection();
        String sql = "delete from transaction_category where transaction_category_id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public TransactionCategory findById(String transactionId) {
        Connection connection = DatabaseConnector.createConnection();
        String sql = "select * from transaction_category where transaction_category_id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, transactionId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new TransactionCategory(resultSet.getString("transaction_category_id"), resultSet.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addNew(String id, String name) {
        Connection connection = DatabaseConnector.createConnection();
        String sql = "insert into transaction_category values (?, ?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, id);
            preparedStatement.setString(2, name);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
