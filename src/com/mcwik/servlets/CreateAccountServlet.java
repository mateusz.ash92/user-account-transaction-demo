package com.mcwik.servlets;

import com.mcwik.pojos.Account;
import com.mcwik.pojos.User;
import com.mcwik.services.AccountService;
import com.mcwik.services.UserService;
import com.mcwik.utils.IdGenerator;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

@WebServlet("/create-account")
public class CreateAccountServlet extends HttpServlet {
    @EJB
    UserService userService;
    @EJB
    AccountService accountService;
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        String name = req.getParameter("name");
        String balance = req.getParameter("balance");
        BigDecimal balanceDecimal = new BigDecimal(balance);

        String userLogin = (String) req.getSession().getAttribute("login");
        User user = userService.findUserByLogin(userLogin);
        String userId = user.getId();

        Account account = new Account(IdGenerator.generateId(), name, balanceDecimal, userId);
        accountService.addNew(account, userId);

        resp.sendRedirect(resp.encodeRedirectURL("user-accounts"));
    }
}
