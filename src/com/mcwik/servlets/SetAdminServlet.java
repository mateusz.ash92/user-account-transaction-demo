package com.mcwik.servlets;

import com.mcwik.pojos.User;
import com.mcwik.services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/adminRole")
public class SetAdminServlet extends HttpServlet {
    @EJB
    private UserService userService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        User user = userService.findUserById(id);
        userService.updateUser(user.getId(), user.getName(), user.getSurname(), user.getLogin(), user.getPassword(),
                "admin", user.getEmail());
        resp.sendRedirect(resp.encodeRedirectURL("showUsersToMe"));
    }
}
