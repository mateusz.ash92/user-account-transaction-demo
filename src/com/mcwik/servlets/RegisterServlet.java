package com.mcwik.servlets;

import com.mcwik.pojos.User;
import com.mcwik.services.UserService;
import com.mcwik.utils.IdGenerator;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
    @EJB
    private UserService userService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        String id = IdGenerator.generateId();
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String role = req.getParameter("user");
        String email = req.getParameter("email");
        boolean isDataCorrect = validate(id, name, surname, login, password, role, email);

        if (isDataCorrect) {
            if (userWithThisIdExists(id)) {
                resp.setStatus(HttpServletResponse.SC_CONFLICT);
                resp.getWriter().write("User with this ID exists!");
            } else if (userWithThisLoginExists(login)) {
                resp.setStatus(HttpServletResponse.SC_CONFLICT);
                resp.getWriter().write("User with this login exists!");
            }else {
                User user = new User(id, name, surname, login, password, role, email);
                userService.addNewUser(user);
                resp.getWriter().write("User successfully added");
            }
        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().write("Fill all the information");
        }
    }
    private boolean validate(String id, String name, String surname, String login, String password, String role, String email) {
        return id != null && !id.isEmpty() && name != null && !name.isEmpty() && surname != null && !surname.isEmpty()
                && login != null && !login.isEmpty() && password != null && !password.isEmpty() && role != null
                && !role.isEmpty() && email != null && !email.isEmpty();
    }
    private boolean userWithThisLoginExists(String login) {
        for (User user : userService.getAllUsers()) {
            if (login.equals(user.getLogin())) {
                return true;
            }
        }
        return false;
    }
    private boolean userWithThisIdExists(String id) {
        for (User user : userService.getAllUsers()) {
            if (id.equals(user.getId())) {
                return true;
            }
        }
        return false;
    }
}
