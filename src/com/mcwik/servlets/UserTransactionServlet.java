package com.mcwik.servlets;

import com.mcwik.pojos.Transaction;
import com.mcwik.pojos.User;
import com.mcwik.services.TransactionService;
import com.mcwik.services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/user-transactions")
public class UserTransactionServlet extends HttpServlet {
    @EJB
    private UserService userService;
    @EJB
    private TransactionService transactionService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = (String) req.getSession(false).getAttribute("login");
        User user = userService.findUserByLogin(login);
        List<Transaction> transactions = transactionService.findById(user.getId());
        req.setAttribute("transactions", transactions);
        req.getRequestDispatcher("user-transactions.jsp").forward(req, resp);
    }
}
