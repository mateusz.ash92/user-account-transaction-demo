package com.mcwik.servlets;

import com.mcwik.pojos.User;
import com.mcwik.services.UserService;
import com.mcwik.utils.IdGenerator;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@WebServlet("/users")
public class UserServlet extends HttpServlet {
    @EJB
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setUTF8(req, resp);
        String id = req.getParameter("id");
        String login = req.getParameter("login");
        if (id == null && login == null) {
            resp.getWriter().write(userService.getAllUsers().toString());
        } else if (id != null) {
            userService.getAllUsers().stream().
                    filter(u -> id.equals(u.getId())).
                    forEach(u -> writeUser(u, resp));
            if (login != null) {
                userService.getAllUsers().stream().
                        filter(u -> login.equals(u.getLogin())).
                        forEach(u -> writeUser(u, resp));
            }
        } else if (login != null) {
            userService.getAllUsers().stream().
                    filter(u -> login.equals(u.getLogin())).
                    forEach(u -> writeUser(u, resp));
        }
    }

    private void writeUser(User u, HttpServletResponse resp) {
        try {
            resp.getWriter().write(u.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setUTF8(req, resp);
        String id = IdGenerator.generateId();
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String role = "user";
        String email = req.getParameter("email");
        boolean isDataCorrect = validate(id, name, surname, login, password, role, email);
        if (isDataCorrect) {
            if (userWithThisIdExists(id)) {
                resp.setStatus(HttpServletResponse.SC_CONFLICT);
                resp.getWriter().write("User with this ID exists!");
            } else if (userWithThisLoginExists(login)) {
                resp.setStatus(HttpServletResponse.SC_CONFLICT);
                resp.getWriter().write("User with this login exists!");
            }else {
                User user = new User(id, name, surname, login, password, role, email);
                userService.addNewUser(user);
                resp.getWriter().write("User successfully added");
                resp.getWriter().write("<br><a href=\"login.html\">Login</a>");
            }
        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().write("Fill all the information");
        }
    }

    private boolean userWithThisIdExists(String id) {
        for (User user : userService.getAllUsers()) {
            if (id.equals(user.getId())) {
                return true;
            }
        }
        return false;
    }
    private boolean userWithThisLoginExists(String login) {
        for (User user : userService.getAllUsers()) {
            if (login.equals(user.getLogin())) {
                return true;
            }
        }
        return false;
    }

    private boolean validate(String id, String name, String surname, String login, String password, String role, String email) {
        return id != null && !id.isEmpty() && name != null && !name.isEmpty() && surname != null && !surname.isEmpty()
                && login != null && !login.isEmpty() && password != null && !password.isEmpty() && role != null
                && !role.isEmpty() && email != null && !email.isEmpty();
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setUTF8(req, resp);
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String role = req.getParameter("role");
        String email = req.getParameter("email");
        boolean isDataCorrect = validate(id, name, surname, login, password, role, email);
        boolean isUpdated = false;

        if (isDataCorrect) {
            for (User user : userService.getAllUsers()) {
                if (id.equals(user.getId())) {
                    userService.updateUser(id, name, surname, login, password, role, email);
                    resp.getWriter().write("User successfully updated");
                    isUpdated = true;
                }
            }
            if (!isUpdated) {
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                resp.getWriter().write("User not found");
            }
        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().write("Data is not correct!");
        }
    }

    private void setUTF8(HttpServletRequest req, HttpServletResponse resp) throws UnsupportedEncodingException {
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
    }

    private void updateUser(String name, String surname, String login, String password, String role, String email, User user) {
        user.setName(name);
        user.setSurname(surname);
        user.setLogin(login);
        user.setPassword(password);
        user.setRole(role);
        user.setEmail(email);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setUTF8(req, resp);
        String id = req.getParameter("id");
        if (userWithThisIdExists(id)) {
            userService.deleteUserById(id);
            resp.getWriter().write("User deleted");
        } else {
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            resp.getWriter().write("User not found");
        }
    }
}
