package com.mcwik.servlets;
import com.mcwik.pojos.User;
import com.mcwik.services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/delete-account")
public class DeleteAccountServlet extends HttpServlet {
    @EJB
    UserService userService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("id") != null) {
            HttpSession session = req.getSession();
            String id = req.getParameter("id");
            userService.deleteUserById(id);
            session.invalidate();
            resp.sendRedirect(resp.encodeRedirectURL("homepage.html"));
        } else {
            HttpSession session = req.getSession(false);
            String login = (String) session.getAttribute("login");
            User user = userService.findUserByLogin(login);
            userService.deleteUserById(user.getId());
            session.invalidate();
            resp.sendRedirect(resp.encodeRedirectURL("homepage.html"));
        }
    }
}
