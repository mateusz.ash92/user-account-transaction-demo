package com.mcwik.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/homepage.html")
public class HomepageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");

        resp.getWriter().write("Welcome to HOMEPAGE!<br>");
        resp.getWriter().write("<br><a href=\"register.html\">Registration form</a>");
        resp.getWriter().write("<br><a href=\"showUsersToMe\">Users table</a>");
        resp.getWriter().write("<br><a href=\"all-account\">All accounts</a>");
        resp.getWriter().write("<br><a href=\"user-accounts\">Accounts for user</a>");
        resp.getWriter().write("<br><a href=\"all-transactions\">All transactions</a>");
        resp.getWriter().write("<br><a href=\"user-transactions\">Transactions for user</a>");
        resp.getWriter().write("<br><a href=\"logout\">Logout</a>");
        resp.getWriter().write("<br>");
        resp.getWriter().write("<br><a href=\"create-account.jsp\">CREATE ACCOUNT</a>");
        resp.getWriter().write("<br>");
        resp.getWriter().write("<br><a href=\"delete-account\">Delete account!</a>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
