package com.mcwik.servlets;

import com.mcwik.pojos.Transaction;
import com.mcwik.pojos.User;
import com.mcwik.services.AccountService;
import com.mcwik.services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

@WebServlet("/create-transaction")
public class CreateTransactionServlet extends HttpServlet {
    @EJB
    UserService userService;
    @EJB
    AccountService accountService;
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        String accountId = req.getParameter("accountId");
        String description = req.getParameter("description");
        String amount = req.getParameter("amount");
        BigDecimal amountDecimaal = new BigDecimal(amount);

        String userLogin = (String) req.getSession().getAttribute("username");
        User user = userService.findUserByLogin(userLogin);
        String userId = user.getId();

        Transaction transaction = new Transaction(null, description, amountDecimaal, userId, accountId);
        accountService.addNewTransaction(accountId, transaction);

        resp.sendRedirect(resp.encodeRedirectURL("account-details.jsp?id=" + accountId));
    }
}

