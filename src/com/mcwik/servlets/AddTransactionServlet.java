package com.mcwik.servlets;

import com.mcwik.daos.UserDAO;
import com.mcwik.pojos.Transaction;
import com.mcwik.pojos.User;
import com.mcwik.services.TransactionService;
import com.mcwik.utils.IdGenerator;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

@WebServlet("/addTransaction")
public class AddTransactionServlet extends HttpServlet {
    @EJB
    private UserDAO userDAO;
    @EJB
    private TransactionService transactionService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        String accountId = req.getParameter("id");
        String description = req.getParameter("description");
        String categoryId = req.getParameter("category_id");
        BigDecimal amount = new BigDecimal(req.getParameter("amount"));

        String login = (String) req.getSession().getAttribute("login");
        User userByLogin = userDAO.findUserByLogin(login);
        String userId = userByLogin.getId();

        Transaction transaction = new Transaction(IdGenerator.generateId(), description, amount, categoryId, userId, accountId);

        transactionService.addNew(transaction);
        resp.sendRedirect(resp.encodeRedirectURL("user-transactions"));
    }
}
