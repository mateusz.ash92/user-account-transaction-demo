package com.mcwik.servlets;

import com.mcwik.pojos.Account;
import com.mcwik.pojos.User;
import com.mcwik.services.AccountService;
import com.mcwik.services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@WebServlet("/user-accounts")
public class UserAccountsServlet extends HttpServlet {
    @EJB
    private UserService userService;
    @EJB
    private AccountService accountService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = (String) req.getSession(false).getAttribute("login");
        User user = userService.findUserByLogin(login);
        List<Account> accounts = accountService.findByUserId(user.getId());
        BigDecimal sum = new BigDecimal(0);
        req.setAttribute("accounts", accounts);
        req.setAttribute("sum", sum);
        req.getRequestDispatcher("/user-accounts.jsp").forward(req, resp);
    }
}
