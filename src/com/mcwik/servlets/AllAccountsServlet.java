package com.mcwik.servlets;

import com.mcwik.pojos.Account;
import com.mcwik.services.AccountService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/all-account")
public class AllAccountsServlet extends HttpServlet {
    @EJB
    private AccountService accountService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Account> allAccounts = accountService.findAll();
        req.setAttribute("accounts", allAccounts);
        req.getRequestDispatcher("/all-account.jsp").forward(req, resp);
    }
}
