package com.mcwik.servlets;

import com.mcwik.pojos.Transaction;
import com.mcwik.services.TransactionService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/all-transactions")
public class AllTransactionsServlet extends HttpServlet {
    @EJB
    TransactionService transactionService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Transaction> transactions = transactionService.findAll();
        req.setAttribute("transactions", transactions);
        req.getRequestDispatcher("/all-transactions.jsp").forward(req, resp);
    }
}
