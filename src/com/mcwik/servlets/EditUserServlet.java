package com.mcwik.servlets;

import com.mcwik.pojos.User;
import com.mcwik.services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/edit-user")
public class EditUserServlet extends HttpServlet {
    @EJB
    private UserService userService;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        User userById = userService.findUserById(id);
        req.setAttribute("user", userById);
        req.getRequestDispatcher("/edit-user.jsp").forward(req, resp);
    }
}
