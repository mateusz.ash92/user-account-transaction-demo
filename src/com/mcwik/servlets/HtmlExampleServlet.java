package com.mcwik.servlets;

import com.mcwik.daos.UserDAO;
import com.mcwik.pojos.User;
import com.mcwik.services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/showUsers")
public class HtmlExampleServlet extends HttpServlet {
    @EJB
    private UserDAO userDAO;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<User> users = new ArrayList<>(userDAO.findAll());

        resp.getWriter().write("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Users</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1>Registered Users</h1>\n" +
                "<br>\n" +
                "<table>\n" +
                "    <tr>\n" +
                "        <th>ID</th>\n" +
                "        <th>Imie</th>\n" +
                "        <th>Nazwisko</th>\n" +
                "        <th>Login</th>\n" +
                "        <th>Rola</th>\n" +
                "        <th>Email</th>\n" +
                "    </tr>\n");
        for (User user : users) {
            String id = user.getId();
            String name = user.getName();
            String surname = user.getSurname();
            String login = user.getLogin();
            String role = user.getRole();
            String email = user.getEmail();
            resp.getWriter().write("    <tr>\n" +
                    "        <td>" + id + "</td>\n" +
                    "        <td>" + name + "</td>\n" +
                    "        <td>" + surname + "</td>\n" +
                    "        <td>" + login + "</td>\n" +
                    "        <td>" + role + "</td>\n" +
                    "        <td>" + email + "</td>\n" +
                    "    </tr>\n");
        }
             resp.getWriter().write(     "</table>\n" +
                     "\n" +
                     "</body>\n" +
                     "</html>");
    }
}
