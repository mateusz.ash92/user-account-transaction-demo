package com.mcwik.servlets;

import com.mcwik.daos.UserDAO;
import com.mcwik.pojos.User;
import com.mcwik.services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @EJB
    private UserService userService;
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        User user = userService.findUserByLogin(login);
        if (user != null) {
            if (password.equals(user.getPassword()) && login.equals(user.getLogin())) {
                req.getSession().setAttribute("login", login);
                resp.sendRedirect(resp.encodeRedirectURL("homepage.html"));
            } else {
                resp.sendRedirect(resp.encodeRedirectURL("login.html"));
            }
        } else {
            resp.sendRedirect(resp.encodeRedirectURL("login.html"));
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
