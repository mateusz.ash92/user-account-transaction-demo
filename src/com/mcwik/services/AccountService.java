package com.mcwik.services;

import com.mcwik.daos.AccountDAO;
import com.mcwik.pojos.Account;
import com.mcwik.pojos.Transaction;
import com.mcwik.utils.IdGenerator;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.List;

@Stateless
public class AccountService {
    @EJB
    private AccountDAO accountDAO;
    @EJB
    private TransactionService transactionService;

    public AccountService() {
    }
    public List<Account> findAll() {
        return accountDAO.findAll();
    }
    public List<Account> findByUserId(String id) {
        return accountDAO.findByUserId(id);
    }
    public void addNew(Account account, String userId) {
        accountDAO.addNew(account, userId);
    }

    public Account findByAccountId(String id) {
        return accountDAO.findByAccountId(id);
    }

    public void addNewTransaction(String accountId, Transaction transaction){

        transaction.setId(IdGenerator.generateId());

        Account account = findByAccountId(accountId);
        if(account != null){
            BigDecimal oldBalance = account.getBalance();
            BigDecimal transactionAmount = transaction.getAmount();
            account.setBalance(oldBalance.add(transactionAmount));

            account.getTransactionIds().add(transaction.getId());

            transaction.setAccountId(accountId);

            transactionService.addNew(transaction);
        }
    }
//    public void delete(String id) {
//        accounts.removeIf(account -> id.equals(account.getId()));
//    }
//    public List<Account> getForAccount(String id) {
//        List<Account> accountsForUser = new ArrayList<>();
//        for (Account account: accounts) {
//            if (id.equals(account.getId())) {
//                accountsForUser.add(account);
//            }
//        }
//        return accountsForUser;
//    }
//
//    public List<Account> getAllUserAccounts(String userId) {
//        List<Account> result = new ArrayList<>();
//        for (Account account : accounts) {
//            if(account.getUserIds().contains(userId)){
//                result.add(account);
//            }
//        }
//        return result;
//    }
//
//    public void edit(String id, String name) {
//        for (Account account : accounts) {
//            if (id.equals(account.getId())) {
//                account.setName(name);
//            }
//        }
//    }
//    static void updateBalance(String id, BigDecimal balance) {
//        for (Account account : accounts) {
//            if (id.equals(account.getId())) {
//                account.setBalance(balance);
//            }
//        }
//    }
}
