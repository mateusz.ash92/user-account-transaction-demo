package com.mcwik.services;

import com.mcwik.daos.TransactionDAO;
import com.mcwik.pojos.Transaction;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class TransactionService {

    @EJB
    private TransactionDAO transactionDAO;

    public TransactionService() {
    }

    public List<Transaction> findAll() {
        return transactionDAO.findAll();
    }
    public List<Transaction> findById(String id) {
       return transactionDAO.findById(id);
    }
    public List<Transaction> getForAccount(String id) {
        return transactionDAO.getForAccount(id);
    }
    public void addNew(Transaction transaction) {
        transactionDAO.addNew(transaction);
    }








//    public void deleteTransaction (String id) {
//        transactions.removeIf(transaction -> id.equals(transaction.getId()));
//    }


//    public static List<Transaction> getAllUserTransactions(String userId) {
//        List<Transaction> userTransactions = new ArrayList<>();
//
//        for (Transaction transaction : transactions) {
//            if(transaction.getUserId().equals(userId)){
//                userTransactions.add(transaction);
//            }
//        }
//        return userTransactions;
//    }
//    public static Transaction getById(String transactionId) {
//        for (Transaction transaction : transactions) {
//            if(transaction.getId().equals(transactionId)){
//                return transaction;
//            }
//        }
//
//        return null;
//    }



}
