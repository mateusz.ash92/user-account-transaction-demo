package com.mcwik.services;

import com.mcwik.daos.TransactionCategoryDAO;
import com.mcwik.pojos.TransactionCategory;
import com.mcwik.utils.IdGenerator;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class TransactionCategoryService {
    private static List<TransactionCategory> transactionCategories = new ArrayList<>();
    @EJB
    private TransactionCategoryDAO transactionCategoryDAO;


    public TransactionCategoryService() {
    }

    public List<TransactionCategory> getAll() {
        return transactionCategoryDAO.getAll();
    }


    public void deleteById(String id) {
        transactionCategoryDAO.deleteById(id);
    }

    public TransactionCategory findById(String id) {
        return transactionCategoryDAO.findById(id);
    }

    public void createNew(String id, String name) {
        transactionCategoryDAO.addNew(id, name);
    }

//    public TransactionCategory getCategoryOfTransaction(String transactionId) {
//        // pobranie transakcji o danym ID
//        Transaction transaction = TransactionService.getById(transactionId);
//        // jeśli istnieje taka transakcja...
//        if (transaction != null) {
//            // pobranie ID kategorii transakcji
//            String categoryId = transaction.getCategoryId();
//            // jeśli jest ustawiona kategoria dla transakcji...
//            if (categoryId != null) {
//                // zwrót kategorię o tym ID
//                return getById(categoryId);
//            }
//        }
//
//        return null;
//    }

    private TransactionCategory getById(String categoryId) {
        for (TransactionCategory transactionCategory : transactionCategories) {
            if (transactionCategory.getId().equals(categoryId)) {
                return transactionCategory;
            }
        }
        return null;


    }
    public void createNew(String name) {
        // wylosowanie ID
        String randomId = IdGenerator.generateId();
        // stworzenie nowego obiektu kategorii z podaną nazwa
        TransactionCategory transactionCategory = new TransactionCategory(randomId, name);
        // dodanie do listy kategorii
        transactionCategories.add(transactionCategory);
    }

    public void delete(String id) {
        transactionCategories.removeIf(tc -> tc.getId().equals(id));
    }


}
