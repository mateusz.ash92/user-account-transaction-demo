package com.mcwik.services;

import com.mcwik.daos.UserDAO;
import com.mcwik.pojos.User;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class UserService {
    @EJB
    private UserDAO userDAO;

    public UserService() {
    }
    public List<User> getAllUsers() {
        return userDAO.findAll();
    }
    public User findUserById(String id) {
        return userDAO.findById(id);
    }
    public User findUserByLogin(String login) {
        return userDAO.findUserByLogin(login);
    }
    public void addNewUser(User user) {
        userDAO.addUser(user);
    }
    public void updateUser(String id, String name, String surname, String login, String password, String role, String email) {
        userDAO.updateUser(id, name, surname, login, password, role, email);
    }
    public void deleteUserById(String id) {
        userDAO.deleteUserById(id);
    }






    //    private static void updateAccountIds(User user) {
//        List<Account> accounts = AccountService.getAllUserAccounts(user.getId());
//        List<String> accountIds = new ArrayList<>();
//        for (Account account : accounts) {
//            accountIds.add(account.getId());
//        }
//        user.setAccountIds(accountIds);
//    }

    //    private static void updateTransactionIds(User user) {
//        List<Transaction> transactions = TransactionService.getAllUserTransactions(user.getId());
//        List<String> transactionIds = new ArrayList<>();
//        for (Transaction transaction : transactions) {
//            transactionIds.add(transaction.getId());
//        }
//        user.setTransactionIds(transactionIds);
//    }


}
