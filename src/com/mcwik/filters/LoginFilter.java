package com.mcwik.filters;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class LoginFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String login = (String) req.getSession().getAttribute("login");
        if (login != null ||
                req.getRequestURI().endsWith("login.html") ||
                req.getRequestURI().endsWith("login") ||
                req.getRequestURI().endsWith("register") ||
                req.getRequestURI().endsWith("register.html") ||
                req.getRequestURI().endsWith("users")) {
            chain.doFilter(req, res);
        }
        else {
            res.sendRedirect(res.encodeRedirectURL("login.html"));
        }
    }
}
