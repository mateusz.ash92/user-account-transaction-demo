<%--
  Created by IntelliJ IDEA.
  User: ubuntu
  Date: 05.10.2019
  Time: 10:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create transaction</title>
</head>
<body>

<%
    String accountId = request.getParameter("accountId");
%>

<h1>Create new transaction for account <b><%=accountId%></b></h1>

<form method="post" action="create-transaction">
    <input type="hidden" value="<%=accountId%>" name="accountId">

    <label>
        Description:
        <input type="text" name="description">
    </label>
    <br>

    <label>
        Amount:
        <input type="number" name="amount" value="0">
    </label>
    <br>

    <input type="submit" value="Create">

</form>

</body>

</html>
