<%@ page import="com.mcwik.pojos.User" %>
<%@ page import="sun.nio.cs.US_ASCII" %>
<%@ page import="com.mcwik.services.UserService" %><%--
  Created by IntelliJ IDEA.
  User: ubuntu
  Date: 28.09.2019
  Time: 16:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit</title>
</head>
<body>
<form method="post" action="edit-account">
    <%
        User user = (User) request.getAttribute("user");
        out.write("<input name=\"id\" value=" + user.getId() + " type=\"hidden\">" +
                "<input name=\"role\" value=" + user.getRole() + " type=\"hidden\">" +
                "<input name=\"login\" value=" + user.getLogin() + " type=\"hidden\">" +
                "     Name:\n" +
                "    <input type=\"text\" name=\"name\" value=\"" + user.getName() +"\">\n" +
                "    <br>" +
                "    Surname:\n" +
                "    <input type=\"text\" name=\"surname\" value=\""+ user.getSurname() +"\">\n" +
                "    <br>" +
                "    Password:\n" +
                "    <input type=\"password\" name=\"password\" value=\""+ user.getPassword() +"\">\n" +
                "    <br>" +
                "    E-mail:\n" +
                "    <input type=\"text\" name=\"email\" value=\""+ user.getEmail() +"\">\n" +
                "    <br>");
    %>
    <input type="submit" value="Edit User">
</form>
</body>
</html>
