<%@ page import="com.mcwik.pojos.Transaction" %>
<%@ page import="java.util.List" %>
<%@ page import="com.mcwik.daos.TransactionDAO" %>
<%@ page import="com.mcwik.pojos.User" %>
<%@ page import="com.mcwik.services.UserService" %><%--
  Created by IntelliJ IDEA.
  User: ubuntu
  Date: 03.10.2019
  Time: 19:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Transactions</title>
</head>
<body>
<%
    List<Transaction> transactionsByUserId = (List<Transaction>) request.getAttribute("transactions");
%>
<h1>Transactions for user</h1>
<br>
<table>
    <tr>
        <th>ID</th>
        <th>Description</th>
        <th>Amount</th>
        <th>CategoryID</th>
        <th>AccountID</th>
        <th>UserID</th>
        <th>Date</th>
    </tr>
    <% for (Transaction transaction : transactionsByUserId) {
    %>
    <tr>
        <th><%=transaction.getId()%></th>
        <th><%=transaction.getDescription()%></th>
        <th><%=transaction.getAmount()%></th>
        <th><%=transaction.getCategoryId()%></th>
        <th><%=transaction.getAccountId()%></th>
        <th><%=transaction.getUserId()%></th>
        <th><%=transaction.getDate()%></th>
    </tr>
    <%}%>
    <br>
</table>
<hr>
<a href="homepage.html"><< Back to homepage</a>
</body>
</html>
