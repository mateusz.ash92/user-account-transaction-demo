<%@ page import="com.mcwik.pojos.Account" %>
<%@ page import="com.mcwik.services.AccountService" %>
<%@ page import="com.mcwik.pojos.Transaction" %>
<%@ page import="java.util.List" %>
<%@ page import="com.mcwik.services.TransactionService" %><%--
  Created by IntelliJ IDEA.
  User: ubuntu
  Date: 05.10.2019
  Time: 10:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Account details</title>
</head>
<body>

<h1>Account details</h1>

<%
    String accountId = request.getParameter("id");
    Account account = AccountService.findByAccountId(accountId);
%>
<b>Id:</b> <%=accountId%>
<br>
<b>Name:</b> <%=account.getName()%>
<br>
<b>Balance:</b> <%=account.getBalance()%>

<hr>

<h3>Transactions:</h3>

<table>
    <tr>
        <th>ID</th>
        <th>Amount</th>
        <th>Description</th>
    </tr>
    <%
        List<Transaction> transactions = TransactionService.getForAccount(accountId);
        for (Transaction transaction : transactions) {
    %>
    <tr>
        <td><%=transaction.getId()%></td>
        <td><%=transaction.getAmount()%></td>
        <td><%=transaction.getDescription()%></td>
    </tr>
    <%
        }
    %>
</table>

<hr>
<a href="create-transaction.jsp?accountId=<%=accountId%>">Add new transaction</a>

<hr>
<a href="user-accounts.jsp"><< Back to accounts</a>

</body>

</html>
